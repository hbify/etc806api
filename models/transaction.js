'use strict';

module.exports = (sequelize, DataTypes) => {
    const Transaction = sequelize.define('Transaction', {
        transactionId: DataTypes.STRING,
        transactionType: DataTypes.STRING,
        amount: DataTypes.DOUBLE,
        currency: DataTypes.STRING,
        cRatio: DataTypes.DOUBLE,
        tDate: { type: DataTypes.DATE, defaultValue: Date.now },
      },
      {
        paranoid: true
      }
    );
    
      return Transaction;
}