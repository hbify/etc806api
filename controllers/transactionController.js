const models = require('../models');
const Transaction = models.Transaction;
const Gift = models.Gift;
const axios = require('axios')


module.exports = {
    send(req, res, next){
        const transaction = {
            transactionId: Math.floor(Math.random() * (99999 - 1000) ) + 1000,
            transactionType: req.body.type,
            amount: req.body.amount,
            currency: req.body.currency,
            cRatio: req.body.cRatio,
            UserId: req.body.UserId,
        };
        
        let User = models.User;
        const url = req.body.url;
        User.findById(req.body.UserId).then(user => {
            if(user.balance > req.body.amount){
                user.balance -= req.body.amount;
                axios.get(url)//'http://192.168.1.67/goip_send_ussd.html?username=root&password=root&port=2&ussd=*100%23'
                    .then(function (response) {
                        if(response.data.code === 0){  
                            user.save().then((usr) => {
                                Transaction.create(transaction).then((transaction)=>{
                                    Gift.create({amountEtb: req.body.amount, ussd: url}).then(()=>{})
                                    res.send(transaction);
                                });
                                
                            }).catch((err)=>{
                                //res.send({error: err});
                            })
                        }else{
                            user.balance += req.body.amount;
                            res.send("something is wrong try again later");
                        }

                });
            }else{
                    res.send("you don't have enough money");
            }               
        }).catch((err)=>{
            //res.send({error: err});
        })
    },
    buy(req, res, next){

        const transaction = {
            transactionId: Math.floor(Math.random() * (99999 - 1000) ) + 1000,
            transactionType: req.body.type,
            amount: req.body.amount,
            currency: req.body.currency,
            cRatio: req.body.cRatio,
            UserId: req.body.UserId
        };
        Transaction.create(transaction).then((transaction)=>{
            let User = models.User;
            let Voucher = models.Voucher;
            let Purchase = models.Purchase;
            const type = transaction.transactionType;
            const etb = transaction.amount *  transaction.cRatio
            User.findById(transaction.UserId).then(user => {
                if( type === "deposit"){
                    user.balance += etb;
                    user.save().then((usr) => {
                        Voucher.findOne({where: {UserId: usr.id, TransactionId: null }}).then((v)=>{
                            
                            const purchase = {
                                purchaseType: "voucher",
                                amount: v.amount,
                                currency: v.currency,
                                cRatio: v.cRatio,
                                UserId: usr.id
                            };
                            Purchase.create(purchase).then((p)=>{
                                if(!v.TransactionId){
                                    v.TransactionId = transaction.id;
                                }
                                v.PurchaseId = p.id;
                                v.save().then((vc)=>{
                                    //res.send({success: "voucher status set to used", voucher: vc})
                                });
                            })
                            
                        })
                        //create purchase
                    })
                }

            });
            res.send(transaction);
        });
    },
    update(req, res, next) {
        const transactionId = req.body.id;
        const transactionUpdate = req.body.transaction;
      
        Transaction.findById(transactionId).then(transaction => {
          transaction.update(transactionUpdate).then((tran) => {
            res.send(tran);
          })
        });
      }

}