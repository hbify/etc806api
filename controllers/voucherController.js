const models = require('../models');
const voucher_codes = require('../services/voucherGenerator');
const Voucher = models.Voucher;

module.exports = {
    index(req, res, next){
        Voucher.findAll().then((vouchers)=>{
            res.send(vouchers);
        })
    },
    generate(req, res, next){
        const codes = voucher_codes.generate({
                            length: req.body.length,
                            count: req.body.count,
                            prefix: "ETC10"
                        });
        const vouchers = codes.map((code) => {
            v = {
                name: req.body.name,
                code: code,
                amount: req.body.amount,
                currency: req.body.currency,
                cRatio: req.body.cRatio,
                status: 3
            };
            return v;
        })
        Voucher.bulkCreate(vouchers).then((vis)=>{
            res.send(vis);
        })
        
    },
    sellToReseller(req, res, next){
        Voucher.findById(req.body.id).then((v)=>{
            if (v.status === 3){
                //status 0 - available, 1 - sold 2 - used, 3 - not available yet
                v.status = 0;
                v.reseller = req.body.UserId;
                v.save().then((vc)=>{
                    res.send({success: "voucher sold to reseller", voucher: vc})
                })
            }
            else{
                res.send({success: "wrong voucher status ", status: vc.status})
            }
            
        })
        //res.send({success: "voucher not found"})
    },
    sellToCustomer(req, res, next){
        Voucher.findById(req.body.id).then((v)=>{
            if (v.status === 0){
                //status 0 - available, 1 - sold 2 - used, 3 - not available yet
                v.status = 1;
                v.UserId = req.body.UserId;
                v.save().then((vc)=>{
                    res.send({success: "voucher sold", voucher: vc})
                })
            }
            else{
                res.send({success: "invalid voucher"})
            }
            
        })
        //res.send({success: "voucher not found"})
    },
    redeem(req, res, next){
        Voucher.findOne({where: {UserId: req.body.UserId, status: 1}}).then((v)=>{
            if (v.status === 1){
                //status 0 - available, 1 - sold, 2 - used, 
                v.status = 2;
                v.save().then((vc)=>{
                    res.send({success: "voucher status set to used", voucher: vc})
                })
            }
            
        }).catch((err)=>{
            res.send({error: err, success: "voucher not valid"})
        })
        //res.send({success: "voucher not found"})
    },
    

}