'use strict';

const bcrypt = require('bcrypt-nodejs');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    firstNames: DataTypes.STRING,
    lastName: DataTypes.STRING,
    password: DataTypes.STRING,
    role: DataTypes.STRING,
    address: DataTypes.STRING,
    city: DataTypes.STRING,
    zip: DataTypes.STRING,
    country: DataTypes.STRING,
    balance: {
      type: DataTypes.DOUBLE,
      defaultValue: 0,
    },
  });

  User.prototype.comparePassword = function(candidatePassword, callback) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
      if (err) { return callback(err); }
  
      callback(null, isMatch);
    });
  };

  User.associate = function(models) {
    models.User.hasMany(models.Transaction);
    models.User.hasMany(models.Voucher);
    models.User.hasMany(models.Purchase);
  }; 

  return User;
};
