'use strict';

module.exports = (sequelize, DataTypes) => {
    const Voucher = sequelize.define('Voucher', {
        name: DataTypes.STRING,
        code: DataTypes.STRING,
        amount: DataTypes.DOUBLE,
        currency: DataTypes.STRING,
        cRatio: DataTypes.DOUBLE,
        status: DataTypes.INTEGER,
        uses: {
            type: DataTypes.INTEGER,
            defaultValue: 1,
          },
        maxUses: {
            type: DataTypes.INTEGER,
            defaultValue: 1,
          },
        maxUserUses: {
            type: DataTypes.INTEGER,
            defaultValue: 1,
          },
        startsAt: {
            type: DataTypes.DATE,
            defaultValue: Date.now,
        },
        expiresAt: {
            type: DataTypes.DATE,
        },

      },
      {
        paranoid: true
      }
    );

    Voucher.associate = function(models) {
        models.Voucher.belongsTo(models.Transaction);
        models.Voucher.belongsTo(models.Purchase);
        models.Voucher.belongsTo(models.User, {foreignKey: 'reseller'});
      };
      
      return Voucher;
}