'use strict';

module.exports = (sequelize, DataTypes) => {
    const Gift = sequelize.define('Gift', {
        name: DataTypes.STRING,
        type: DataTypes.STRING,
        amountEtb: DataTypes.DOUBLE,
        ussd: DataTypes.STRING,
        toPhone: DataTypes.STRING,
        status: DataTypes.STRING,
        gDate: DataTypes.DATE,
      });

    Gift.associate = function(models) {
        models.Gift.belongsTo(models.Transaction);
      };

      return Gift;
}