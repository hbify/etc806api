'use strict';

module.exports = (sequelize, DataTypes) => {
    const Purchase = sequelize.define('Purchase', {
        purchaseId: DataTypes.STRING,
        purchaseType: DataTypes.STRING,
        amount: DataTypes.DOUBLE,
        currency: DataTypes.STRING,
        cRatio: DataTypes.DOUBLE,
        pDate: { type: DataTypes.DATE, defaultValue: Date.now },
      });
    
      return Purchase;
}