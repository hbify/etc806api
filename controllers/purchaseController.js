const models = require('../models');
const Purchase = models.Purchase;

module.exports = {

    index(req, res, next){
        Purchase.findAll().then((purchases)=>{
            res.send(purchases);
        })
    },
    purchases(req, res, next) {
        const id = req.body.id;
      
        Purchase.findAll({where : {UserId : id}}).then(pr => {
            res.send(pr); 
        });
      }
}