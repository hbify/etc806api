

const randomInt = (min, max) => {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

const randomElem = (arr) => {
        return arr[randomInt(0, arr.length - 1)];
    }

 const charset = (name) => {
        var charsets = {
            numbers: "0123456789",
            alphabetic: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
            alphanumeric: "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        };
        return charsets[name];
    }

const repeat = (str, count) =>  {
        var res = "";
        for (var i = 0; i < count; i++) {
            res += str;
        }
        return res;
    }

const Config = (config) => {
        config = config || {};
        this.count = config.count || 1;
        this.length = config.length || 8;
        this.charset = config.charset || charset("alphanumeric");
        this.prefix = config.prefix || "";
        this.postfix = config.postfix || "";
        this.pattern = config.pattern || repeat("#", this.length);
    }

const generateOne = (config) => {
        var code = config.pattern.split('').map(function(char) {
            if (char === '#') {
                return randomElem(config.charset);
            } else {
                return char;
            }
        }).join('');
        return config.prefix + code + config.postfix;
    }

const isFeasible = (charset, pattern, count) => {
        return Math.pow(charset.length, (pattern.match(/#/g) || []).length) >= count;
    }

const generate = (config) => {
        config = new Config(config);
        var count = config.count;
        if (!isFeasible(config.charset, config.pattern, config.count)) {
            throw new Error("Not possible to generate requested number of codes.");
        }

        var codes = {};
        while (count > 0) {
            var code = generateOne(config);
            if (codes[code] === undefined) {
                codes[code] = true;
                count--;
            }
        }
        return Object.keys(codes);
    }

module.exports = generate;
    