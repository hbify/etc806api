const Authentication = require('./controllers/authentication');
const transactionController = require('./controllers/transactionController');
const voucherController = require('./controllers/voucherController');
const purchaseController = require('./controllers/purchaseController');
const giftController = require('./controllers/giftController');
const passportService = require('./services/passport');
const passport = require('passport');

const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignin = passport.authenticate('local', { session: false });

module.exports = function(app) {
  app.get('/', requireAuth, function(req, res) {
    res.send({ message: 'Super secret code is ABC123' });
  });
  app.post('/signin', requireSignin, Authentication.signin);
  app.post('/signup', Authentication.signup);
  app.post('/send', requireAuth, transactionController.send);
  app.post('/buy', requireAuth, transactionController.buy);
  app.put('/update', requireAuth, Authentication.update);
  app.post('/user', requireAuth, Authentication.user);
  app.get('/users', requireAuth, Authentication.users);
  app.put('/user/update', requireAuth, Authentication.update);
  app.delete('/user/remove', requireAuth, Authentication.remove);
  app.post('/voucher/generate', requireAuth, voucherController.generate);
  app.get('/vouchers', requireAuth, voucherController.index);
  app.post('/voucher/retail', requireAuth, voucherController.sellToReseller);
  app.post('/voucher/sell', requireAuth, voucherController.sellToCustomer);
  app.post('/voucher/redeem', requireAuth, voucherController.redeem);
  app.put('/transaction', requireAuth, transactionController.update);
  app.get('/purchases', requireAuth, purchaseController.index);
  app.post('/purchase', requireAuth, purchaseController.purchases);
  app.get('/gifts', requireAuth, giftController.index);
}
